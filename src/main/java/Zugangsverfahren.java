import java.util.Arrays;
import java.util.List;

public enum Zugangsverfahren {
    free("frei zugänglich"), //
    registration("Anmeldung erforderlich"), //
    candidator("Bewerbung erforderlich"); //


    private final String status;

    private Zugangsverfahren(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }

    public static List<String> getAllZugangsverfahren() {
        return Arrays.asList( //
                free.toString(), //
                registration.toString(), //
                candidator.toString()); //
    }
}
