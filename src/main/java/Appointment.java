
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Appointment {

    private int id;
    private Date beginn, end;
    private String title, place, note, region, bereich, gruppe, zugangsverfahren;
    private List<messages> messages  = new ArrayList<>();

    private List<Participant> participants = new ArrayList<>();

    public Appointment(){

    }

    public Appointment(int id, Date beginn, Date end, String title, String place, String note, String region, String bereich, String gruppe, String zugangsverfahren) {
        this.id = id;
        this.beginn = beginn;
        this.end = end;
        this.title = title;
        this.place = place;
        this.note = note;
        this.region = region;
        this.bereich = bereich;
        this.gruppe = gruppe;
        this.zugangsverfahren = zugangsverfahren;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getBereich() {
        return bereich;
    }

    public void setBereich(String bereich) {
        this.bereich = bereich;
    }

    public String getGruppe() {
        return gruppe;
    }

    public void setGruppe(String gruppe) {
        this.gruppe = gruppe;
    }


    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    public Date getBeginn() {
        return beginn;
    }

    public void setBeginn(Date beginn) {
        this.beginn = beginn;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getZugangsverfahren() {
        return zugangsverfahren;
    }

    public void setZugangsverfahren(String zugangsverfahren) {
        this.zugangsverfahren = zugangsverfahren;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public List<messages> getMessages(){
        return messages;
    }

    public void addMessage(String message){
        messages.add(new messages(UserHelper.getCurrentUser(), message));
    }
}
