import org.joda.time.DateTime;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

import javax.faces.context.FacesContext;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class CalendarBean {
    private List<Appointment> appointmentList;
    private List<Appointment> dayAppointmentlist = new ArrayList<>();
    private Appointment activeAppointment;
    private List<User> participants = new ArrayList<>();
    private ScheduleModel eventModel;
    private ScheduleEvent event = new DefaultScheduleEvent();
    private boolean showPrivate = true;
    private boolean showPublic = true;
    private boolean showDayList = false;



    private String newMessage;


    public Appointment getNewAppointment() {
        return activeAppointment;
    }

    public void setNewAppointment(Appointment newAppointment) {
        this.activeAppointment = newAppointment;
    }

    public List<Appointment> getAppointmentList() {
        if(appointmentList == null){
            appointmentList = DummyDB.getAppointmentList();
        }
       return appointmentList;
    }

    public void setAppointmentList(List<Appointment> appointmentList) {
        this.appointmentList = appointmentList;
        DummyDB.setAppointmentList(appointmentList);
    }

    public String addAppointment() {
        activeAppointment.getParticipants().add(new Participant(UserHelper.getCurrentUser(), Teilnahmestatus.yes, true));
        appointmentList.add(activeAppointment);
        DummyDB.setAppointmentList(appointmentList);
        return "success";
    }

    public void removeAppointment(Appointment appointment) {
        appointmentList.remove(appointment);
        DummyDB.setAppointmentList(appointmentList);
    }

    public Timestamp getCurrentDate() {
        return new Timestamp(System.currentTimeMillis());
    }

    public List<String> getZugangsverfahren() {
        return Zugangsverfahren.getAllZugangsverfahren();
    }

    public List<User> getAllUser() {
        return DummyDB.getUserWithoutCurrent();
    }

    public void setParticipants(List<String> userList) {
        List<Participant> pList = new ArrayList<>();
        for (String id : userList) {
            User u = DummyDB.getUserByID(Integer.parseInt(id));
            if (u != null) {
                Participant p = new Participant(u.getId(), null, false);
                pList.add(p);
            }
        }
        activeAppointment.setParticipants(pList);
    }

    public void setParticipants(int userList) {
        List<Participant> pList = new ArrayList<>();
        User u = DummyDB.getUserByID(userList);
        Participant p = new Participant(u.getId(), null, false);
        pList.add(p);
        activeAppointment.setParticipants(pList);
    }

    public List<Participant> getParticipants() {
        return activeAppointment.getParticipants();
    }

    public List<Integer> getParticipantsId() {
        List<Integer> idList = new ArrayList<>();
        for (Participant p : activeAppointment.getParticipants()) {
            idList.add(p.getUser().getId());
        }
        return idList;
    }

    public String setEditAppointment(int id) {
        activeAppointment = DummyDB.getAppointmentByID(id);
        return "edit";
    }

    public Appointment getEditAppointment() {
        return activeAppointment;
    }

    public String createNewAppointment() {
        activeAppointment = new Appointment();
        activeAppointment.setId(getAppointmentList().size());
        return "newEvent";
    }

    public String saveEditAppointment() {
        return "success";
    }


    public Appointment getShowAppointment() {
        return activeAppointment;
    }

    public String setShowAppointment(int id) {
        activeAppointment = DummyDB.getAppointmentByID(id);
        return "show";
    }

    public String sendMessage() {
        activeAppointment.addMessage(newMessage);
        return "message_send";
    }

    public void setNewMessage(String newMessage) {
        this.newMessage = newMessage;
    }

    public String getNewMessage() {
        newMessage = "";
        return newMessage;
    }

    public String setTeilnahme(String teilnahme) {
        for (Participant p : activeAppointment.getParticipants()
                ) {
            if (p.getUser().getId() == UserHelper.getCurrentUser().getId()) {
                p.setTeilnahmestatus(Teilnahmestatus.valueOf(teilnahme));
            }

        }
        return "reload";
    }

    public boolean renderTeilnahmechoice() {
        for (Participant p : activeAppointment.getParticipants()
                ) {
            if (p.getUser().getId() == UserHelper.getCurrentUser().getId()) {
                if (p.getTeilnahmestatus() == null) return true;
                else return false;
            }
        }
        return false;
    }

    public String getCurrentTeilnahme() {
        for (Participant p : activeAppointment.getParticipants()
                ) {
            if (p.getUser().getId() == UserHelper.getCurrentUser().getId()) {
                if (p.getTeilnahmestatus() == null) return null;
                else return p.getTeilnahmestatus().toString();
                //ToDo: Make Ajax for changing
            }
        }
        return null;
    }

    public ScheduleModel getEventModel() {
        if(showPrivate == true && showPublic == true) return DummyDB.getEventModel();
        else if (showPrivate == true) return DummyDB.getPrivateEventModel();
        else return DummyDB.getPublicEventModel();
    }


    public String onEventSelect(SelectEvent selectEvent) throws IOException {
        event = (ScheduleEvent) selectEvent.getObject();
        activeAppointment = DummyDB.getAppointmentByTitle(event.getTitle());
        FacesContext.getCurrentInstance().getExternalContext().redirect("showEvent.html");
        FacesContext.getCurrentInstance().responseComplete();

        return "edit";

    }

    public void onDateSelect(SelectEvent selectEvent) {
        event = new DefaultScheduleEvent("", (Date) selectEvent.getObject(), (Date) selectEvent.getObject());
        dayAppointmentlist = DummyDB.getDayAppointments(new DateTime(event.getStartDate()));
        showDayList = true;
    }

    public ScheduleEvent getEvent() {
        return event;
    }

    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }


    public boolean isShowPrivate() {
        return showPrivate;
    }

    public void setShowPrivate(boolean showPrivate) {
        this.showPrivate = showPrivate;
    }

    public boolean isShowPublic() {
        return showPublic;
    }

    public void setShowPublic(boolean showPublic) {
        this.showPublic = showPublic;
    }

    public List<Appointment> getDayAppointmentlist() {
        return dayAppointmentlist;
    }

    public boolean showDayList(){
        return showDayList;
    }
}
