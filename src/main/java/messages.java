import java.text.SimpleDateFormat;
import java.util.Date;

public class messages {

    private int id;
    private String message;
    private User sender;
    private Date created_at;

    public messages(User user, String message){
        this.message = message;
        sender = user;
        created_at = new Date(System.currentTimeMillis());
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public String getCreated_String(){
        return (new SimpleDateFormat("yyyy-MM-dd HH:mm").format(created_at));
    }




}

