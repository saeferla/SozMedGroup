import org.joda.time.DateTime;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleModel;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class DummyDB {

    private static List<User> userList, userWithoutCurrent;
    private static List<Appointment> appointmentList;
    private static ScheduleModel eventModel, privateEventModel, publicEventModel;


    public static List<User> getUsers() {
        if (userList == null) {
            userList = new ArrayList<>();
            User u1 = new User(1, "Max Mustermann");
            User u2 = new User(2, "Peter Lustig");
            User u3 = new User(3, "Sabine Meier");
            User u4 = new User(4, "Bob Baumeister");
            User u5 = new User(5, "Schlaubi Schlumpf");
            userList.add(u1);
            userList.add(u2);
            userList.add(u3);
            userList.add(u4);
            userList.add(u5);
            userWithoutCurrent = new ArrayList<>();
            userWithoutCurrent.addAll(userList);
            userList.add(UserHelper.getCurrentUser());
        }

        return userList;
    }

    public static List<User> getUserWithoutCurrent() {
        getUsers();
        return userWithoutCurrent;
    }


    public static User getUserByID(int id) {
        for (User u : getUsers()
                ) {
            if (u.getId() == id) {
                return u;
            }
        }
        return null;
    }

    public static List<Appointment> getAppointmentList() {
        if (appointmentList == null) {

            /**
             *   private int id;
             *     private Date beginn, end;
             *     private String title, place, note, region, bereich, gruppe, zugangsverfahren;
             *     private List<messages> messages  = new ArrayList<>();
             *
             *     private List<Participant> participants = new ArrayList<>();
             */
            //ToDo: Different Dates
            appointmentList = new ArrayList<>();
            List<Participant> p = new ArrayList<>();
            Participant p1 = new Participant(getUsers().get(1).getId(), Teilnahmestatus.yes, false);
            p.add(p1);
            Participant p2 = new Participant(getUsers().get(2), Teilnahmestatus.yes, false);
            p.add(p2);
            Participant p3 = new Participant(UserHelper.getCurrentUser(), null, false);
            p.add(p3);

            Date s = new Date(System.currentTimeMillis());
            DateTime start = new DateTime(s);


            Appointment a = new Appointment(1, start.toDate(), start.plusHours(3).toDate(), "Aqua Festival", "Mauerpark", "Könnte Regnen", "Berlin", "Infostand", "Öffentlich", Zugangsverfahren.free.toString());
            a.setParticipants(p);
            appointmentList.add(a);
            appointmentList.add(new Appointment(2, start.plusHours(5).toDate(), start.plusHours(7).toDate(), "Info Aktion", "Alexanderplatz", "", "Berlin", "Flyeraktion", "Öffentlich", Zugangsverfahren.candidator.toString()));
            appointmentList.add(new Appointment(3, start.plusDays(-4).toDate(), start.plusHours(1).toDate(), "Orgatreffen", "Frankfurter Allee 23", "Vorbereitung erforderlich", "Berlin", "Organisation", "Privat", Zugangsverfahren.registration.toString()));
            appointmentList.add(new Appointment(4, start.plusDays(-5).toDate(), start.plusDays(-5).plusHours(2).toDate(), "Bezirkstreffen", "Köln, Musterstraße 5", "", "Köln", "Organisation", "Privat", Zugangsverfahren.registration.toString()));

        }

        return appointmentList;
    }

    public static void setAppointmentList(List<Appointment> appointments) {
        appointmentList = appointments;
        updateEventModels();
    }

    public static ScheduleModel getEventModel() {
        if (eventModel == null) {
            updateEventModels();

        }
        return eventModel;
    }

    public static ScheduleModel getPrivateEventModel() {
        return privateEventModel;
    }


    public static Appointment getAppointmentByID(int id) {
        if (appointmentList != null) {
            for (Appointment a : appointmentList) {
                if (a.getId() == id) return a;
            }
            return null;
        } else return null;
    }

    public static Appointment getAppointmentByTitle(String title) {
        if (appointmentList != null) {
            for (Appointment a : appointmentList) {
                if (a.getTitle() == title) return a;
            }
            return null;
        } else return null;
    }

    public static ScheduleModel getPublicEventModel() {
        return publicEventModel;
    }

    private static void updateEventModels() {

        eventModel = new DefaultScheduleModel();
        privateEventModel = new DefaultScheduleModel();
        publicEventModel = new DefaultScheduleModel();

        for (Appointment a : getAppointmentList()
                ) {
            DefaultScheduleEvent ev = new DefaultScheduleEvent(a.getTitle(), a.getBeginn(), a.getEnd());
            ev.setId(Integer.toString(a.getId()));
            eventModel.addEvent(ev);
            if (a.getGruppe() == "Privat") {
                privateEventModel.addEvent(ev);

            } else if (a.getGruppe() == "Öffentlich") {
                publicEventModel.addEvent(ev);
            }
        }


    }

    public static List<Appointment> getDayAppointments(DateTime day){
        DateTime start = day.withTimeAtStartOfDay();
        DateTime end = start.plusDays(1).withTimeAtStartOfDay();
        List<Appointment> dayapp = new ArrayList<>();
        for (Appointment a : getAppointmentList()
                ) {
            if((a.getBeginn().after(start.toDate()) && (a.getBeginn().before(end.toDate()) )) || (a.getEnd().before(end.toDate()) && (a.getEnd().after(start.toDate())  )) || (a.getBeginn().before(start.toDate()) && a.getEnd().after(end.toDate()) )){
                dayapp.add(a);
            }
        }

        return dayapp;
    }


}
