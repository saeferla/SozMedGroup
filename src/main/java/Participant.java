public class Participant {


    private int id;
    private User user;
    private Teilnahmestatus teilnahmestatus;

    private boolean admin;

    public Participant(int user_id, Teilnahmestatus teilnahmestatus, boolean admin){
        user = DummyDB.getUserByID(user_id);
        this.teilnahmestatus = teilnahmestatus;
        this.admin = admin;
    }


    public Participant(User user, Teilnahmestatus teilnahmestatus, boolean admin){
        this.user = user;
        this.teilnahmestatus = teilnahmestatus;
        this.admin = admin;
    }

    public Teilnahmestatus getTeilnahmestatus() {
        return teilnahmestatus;
    }

    public void setTeilnahmestatus(Teilnahmestatus teilnahmestatus) {
        this.teilnahmestatus = teilnahmestatus;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
