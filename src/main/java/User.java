/**
 * User Objekt wird nicht in der Datenbank persistiert sondern Benutzer werden mit Daten aus Abfrage an die Benutzerverwaltung von Pool2
 * generiert
 */

public class User {

    private int id;
    private String name;

    public User(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId(){
        return id;
    }

}
