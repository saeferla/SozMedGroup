import java.util.List;

public final class UserHelper {

    private static User currentUser;
    public static User getCurrentUser(){
        if(currentUser == null){
            currentUser = new User(10, "Peter Pan");
        }
        return currentUser;
    }
}
