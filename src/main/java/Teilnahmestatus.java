import java.util.Arrays;
import java.util.List;


public enum Teilnahmestatus {
    yes("Zusage"), //
    no("Absage"), //
    maybe("Vielleicht"); //


    private final String status;

    private Teilnahmestatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }

    public static List<String> getAllZugangsverfahren() {
        return Arrays.asList( //
                yes.toString(), //
                no.toString(), //
                maybe.toString()); //
    }
}